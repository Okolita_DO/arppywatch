**ARPWatcher**

ARPWatcher is a tool for monitoring ARP traffic in Linux environments. It allows tracking changes in the ARP table and notifies about changes, such as MAC address changes or the appearance of new devices on the network.

**Installation**

To install ARPWatcher, follow these steps:

Install the required dependencies:


`pip install scapy`

Clone the ARPWatcher repository:


`git clone https://gitlab.com/Okolita_DO/arppywatch`

Navigate to the repository directory:


`cd ARPWatcher`

Run ARPWatcher:


`python arpwatcher.py`

**Usage**

ARPWatcher monitors ARP traffic on the specified network interface. To configure and use it, a configuration file is required, which should be specified in the config_file variable in the code. You can find an example configuration file in the arp.conf.example file.

**Contact**

If you have any questions or issues with ARPWatcher, feel free to contact the author:

Email: dimaokolita@gmail.com
