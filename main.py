from scapy.all import sniff, ARP
import os
import subprocess
import logging
from datetime import datetime, timedelta
import socket
import signal
import sys
import ipaddress

class ARPWatcher():
    def __init__(self):

        config_file = "/etc/arppywatch/arp.conf"
        config = self.read_config(config_file)
        if config['ignore']:
            config['ignore']=config['ignore'].split(',')
        if config['allowed_networks']:
            config['allowed_networks']=config['allowed_networks'].split(',')
        log_file = config['log_file']
        if not os.path.exists(log_file):
            open(log_file, "a").close()

        if (config['log_level'] == "DEBUG"):
            logging.basicConfig(filename=log_file, level=logging.DEBUG,
                                format='%(asctime)s - %(levelname)s - %(message)s')
        elif (config['log_level'] == "INFO"):
            logging.basicConfig(filename=log_file, level=logging.INFO,
                                format='%(asctime)s - %(levelname)s - %(message)s')
        elif (config['log_level'] == "WARNING"):
            logging.basicConfig(filename=log_file, level=logging.WARNING,
                                format='%(asctime)s - %(levelname)s - %(message)s')
        elif (config['log_level'] == "ERROR"):
            logging.basicConfig(filename=log_file, level=logging.ERROR,
                                format='%(asctime)s - %(levelname)s - %(message)s')
        elif (config['log_level'] == "CRITICAL"):
            logging.basicConfig(filename=log_file, level=logging.CRITICAL,
                                format='%(asctime)s - %(levelname)s - %(message)s')
        signal.signal(signal.SIGINT, self.signal_handler)
        logging.debug(f"Log type :{config['log_level']}")
        logging.debug(f"allowed_networks :{ config['allowed_networks']}")
        logging.info(f'Script init. Config file :{config_file}')
        arp_cache_file = "/var/lib/arppywatch/" + config['iface'] + ".dat"
        if not os.path.exists(arp_cache_file):
            open(arp_cache_file, "a").close()
        logging.info(f"Interface :{config['iface']}")
        logging.info(f"DAT file  :{arp_cache_file}")
        logging.info(f"Ignore list  :!{config['ignore']}!")
        logging.info(f"allowed_networks list  :!{config['allowed_networks']}!")
        logging.info(f"Mail to :{config['mail']}")
        config['arp_cache'] = arp_cache_file
        arp_entries = self.read_arp_entries(arp_cache_file)
        sniff(prn=lambda pkt: self.arp_monitor(pkt, config, arp_entries), filter="arp", store=0, iface=config['iface'])

    def signal_handler(self,sig, frame):
        logging.error('ARPWATCH stoped')
        sys.exit(0)

    def check_ip_network(self,ip, networks, ignore):
        ip_address = ipaddress.IPv4Address(ip)
        if networks != '':
            logging.debug(f"get {ip}")
            for network in networks:
                network_ip, network_mask = network.split('/')
                network_address = ipaddress.IPv4Network(network, strict=False)
                if ip_address in network_address:
                    return True
        elif networks == '' and  ignore:
            return False
        elif networks == '' and not ignore:
            return True
        return False

    def read_config(self,filename):
        config = {}
        with open(filename, 'r') as f:
            for line in f:
                if line.startswith('#'):
                    continue
                parts = line.strip().split('=')
                if len(parts) == 2:
                    key, value = parts
                else:
                    key, value = parts[0], ''
                config[key.strip()] = value.strip()
        return config

    def get_hostname(self,ip_address):
        try:
            hostname, _, _ = socket.gethostbyaddr(ip_address)
            return hostname
        except socket.herror:
            return "<unknown>"

    def send_email(self,subject, body, receiver_email):
        try:
            logging.debug(f"mail From: ARP Watcher <>\nTo: {receiver_email}")
            email_content = f"From: ARP Watcher <>\nTo: {receiver_email}\nSubject: {subject}\n\n{body}"
            p = subprocess.Popen(["/usr/sbin/sendmail", receiver_email], stdin=subprocess.PIPE)
            p.communicate(input=email_content.encode('utf-8'))
        except Exception as e:
            logging.error(f"Error sending email: {e}")

    def read_arp_entries(self,filename):
        arp_entries = {}
        if os.path.exists(filename):
            with open(filename, 'r') as f:
                for line in f:
                    ip, mac, hostname, timestamp_str = line.strip().split(',')
                    timestamp = datetime.strptime(timestamp_str, '%Y-%m-%d %H:%M:%S')
                    arp_entries[ip] = {'mac': mac, 'hostname': hostname, 'timestamp': timestamp}
        return arp_entries


    def save_arp_entries(self,filename, arp_entries):
        with open(filename, 'r+') as f:
            lines = f.readlines()
            f.seek(0)
            for ip, data in arp_entries.items():
                line = f"{ip},{data['mac']},{data['hostname']},{data['timestamp'].strftime('%Y-%m-%d %H:%M:%S')}\n"
                found = False
                for i, existing_line in enumerate(lines):
                    if existing_line.startswith(ip):
                        lines[i] = line
                        found = True
                        break
                if not found:
                    lines.append(line)
            f.truncate(0)
            f.writelines(lines)

    def arp_monitor(self,pkt, config, arp_entries):
        if ARP in pkt and pkt[ARP].op in [1, 2]:
            arp_src_ip = pkt[ARP].psrc
            arp_src_mac = pkt[ARP].hwsrc
            current_time = datetime.now()
            if not self.check_ip_network(arp_src_ip, config['ignore'],True) and self.check_ip_network(arp_src_ip, config['allowed_networks'],False):
                logging.debug(f"accepted {arp_src_ip}")
                if arp_src_ip in arp_entries:
                    old_entry = arp_entries[arp_src_ip]
                    old_mac = old_entry['mac']
                    old_time = old_entry['timestamp']
                    delta = current_time - old_time
                    if old_mac != arp_src_mac and delta > timedelta(hours=1):
                        self.send_email("mac for ip address changed", f"Hostname: {old_entry['hostname']}\n"
                                                                  f"IP Address: {arp_src_ip}\n"
                                                                  f"Interface: {config['iface']}\n"
                                                                  f"Ethernet Address: {arp_src_mac}\n"
                                                                  f"Ethernet Vendor: <unknown>\n"
                                                                  f"Old Ethernet Address: {old_mac}\n"
                                                                  f"Old Ethernet Vendor: <unknown>\n"
                                                                  f"Timestamp: {current_time.strftime('%A, %B %d, %Y %H:%M:%S %z')}\n"
                                                                  f"Previous Timestamp: {old_time.strftime('%A, %B %d, %Y %H:%M:%S %z')}\n"
                                                                  f"Delta: {delta}\n",
                                   config['email'])
                        logging.warning(f"MAC address for  ip address: {arp_src_ip}  changed.  Hostname: {old_entry['hostname']} Ethernet Address: {arp_src_mac} Old Ethernet Address: {old_mac}")
                        self.save_arp_entries(config['arp_cache'], arp_entries)
                    elif old_mac != arp_src_mac and delta < timedelta(hours=1):
                        self.send_email("Flip-Flop ", f"Hostname: {old_entry['hostname']}\n"
                                                               f"IP Address: {arp_src_ip}\n"
                                                               f"Interface: {config['iface']}\n"
                                                               f"Ethernet Address: {arp_src_mac}\n"
                                                               f"Ethernet Vendor: <unknown>\n"
                                                               f"Old Ethernet Address: {old_mac}\n"
                                                               f"Old Ethernet Vendor: <unknown>\n"
                                                               f"Timestamp: {current_time.strftime('%A, %B %d, %Y %H:%M:%S %z')}\n"
                                                               f"Previous Timestamp: {old_time.strftime('%A, %B %d, %Y %H:%M:%S %z')}\n"
                                                               f"Delta: {delta}\n",
                                   config['email'])
                        logging.warning(f"Flip-Flop Hostname: {old_entry['hostname']} Old Ethernet Address: {old_mac} Ethernet Address: {arp_src_mac}")
                        self.save_arp_entries(config['arp_cache'], arp_entries)
                else:
                    hostname = self.get_hostname(arp_src_ip)
                    self.send_email("New host", f"Hostname: {hostname}\n"
                                                          f"IP Address: {arp_src_ip}\n"
                                                          f"Interface: {config['iface']}\n"
                                                          f"Ethernet Address: {arp_src_mac}\n"
                                                          f"Ethernet Vendor: <unknown>\n"
                                                          f"Timestamp: {current_time.strftime('%A, %B %d, %Y %H:%M:%S %z')}\n",
                               config['email'])
                    logging.info(f"NEW host :  Hostname: {hostname} ip address: {arp_src_ip}\n")
                    arp_entries[arp_src_ip] = {'mac': arp_src_mac, 'timestamp': current_time, 'hostname': hostname}
                    self.save_arp_entries(config['arp_cache'], arp_entries)

            else:
                 logging.debug(f'{arp_src_ip}: ignored or not allowed by config')



if __name__ == '__main__':
    ARPWatcher()

